| AIP | Title                        | Status | Category | Author                                  | Created  |
| --- | ---------------------------- | ------ | -------- | --------------------------------------- | -------- |
| 010 | Early transaction expiration | Draft  | Core     | Ethan Reesor \<ethan.reesor@gmail.com\> | 20230831 |

# Summary

Allow the author of a transaction to specify a deadline after which the
transaction will expire if it is still pending.

# Motivation

Allow users to create tighter timelines around transactions where security might
be an issue.

# Specification

- The transaction may specify a deadline/expiration time. If this is greater
  than the default expiration time (2 weeks), it has no effect.
- If a signature or other message is received after that time, the transaction
  expires.
- If the transaction is still pending when the next major block after the
  deadline occurs, the transaction expires.

# Implementation

- Add a field to the transaction header to specify the deadline.
- Update the block state's pending transaction tracker to keep the transaction,
  not just its ID.
- Update the generation of scheduled expiration events to use the major block
  count instead of the default, if it is less than the default.
- Update the transaction executor to expire the transaction if the deadline has
  passed.

# Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).