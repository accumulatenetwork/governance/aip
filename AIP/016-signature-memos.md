| AIP | Title           | Status | Category | Author                                  | Created  |
| --- | --------------- | ------ | -------- | --------------------------------------- | -------- |
| 006 | Signature memos | Draft  | Core     | Ethan Reesor \<ethan.reesor@gmail.com\> | 20230831 |

# Summary

Add the capability to include a memo and/or metadata with a signature.

# Motivation

There are many scenarios where adding information to a signature can be useful.
For example:

- In human multisig scenarios, it allows signers to state why they accept,
  rejection or abstain from a vote.
- It allows automated systems (e.g. staking) to provide feedback on an input,
  such as why the input was rejected.

# Specification

- Add a memo field and a metadata field to all user signature types.

# Implementation

- Add a `string` memo and `[]byte` metadata field to all user signature types.

This does not need an activation height given it has no impact on validation or
execution logic, since nodes without the new fields since the binary encoding
mechanism is backwards compatible.

That being said, if a signature was sent via JSON to a node that is not aware of
the new fields, those fields will be ignored. So the feature should only be
advertized once every node has updated.

# Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).
