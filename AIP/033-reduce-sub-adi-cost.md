| AIP | Title               | Status | Category | Author                                  | Created  |
| --- | ------------------- | ------ | -------- | --------------------------------------- | -------- |
| 007 | Reduce sub-ADI cost | Draft  | Core     | Ethan Reesor \<ethan.reesor@gmail.com\> | 20230831 |

# Summary

Reduce the cost of sub-ADIs.

# Motivation

The current cost of sub-ADIs makes complex structures potentially cost
prohibitive.

# Specification

- Lower the fee for creating a sub-ADI to 1 credit.
- Make the fee for creating a sub-ADI configurable.

# Implementation

- Add a CreateSubIdentity field to the dynamic fee schedule.
- Update the fee calculator to use that field if it is non-zero, otherwise
  default to the current fee (500 credits).
- Once the update has been deployed, update the dynamic fee schedule, setting
  CreateSubIdentity to 1 credit.

This should not require an activation height as long as we are careful. Since
the implementation outlined above will default to the same behavior until the
new field has been set, as long as we don't do that until all nodes have updated
there should not be any consensus issue.

# Background

Sub-ADIs were not intended to cost as much as a root ADI. The cost of an ADI was
set before sub-ADIs could be created and they inherited the cost of a root ADI
since they use the same transaction.

# Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).