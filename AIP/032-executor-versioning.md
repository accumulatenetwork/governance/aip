| AIP | Title               | Status    | Category | Author                                  | Created  |
| --- | ------------------- | --------- | -------- | --------------------------------------- | -------- |
| 002 | Executor versioning | Last Call | Core     | Ethan Reesor \<ethan.reesor@gmail.com\> | 20221216 |

# Summary

Implement a mechanism to activate transaction and signature execution logic at a
specific block height (via an operator transaction).

# Motivation

The capability to introduce new execution logic without compromising the ability
to replay the network history. In other words, creating a node running the
latest software version and replaying the entire network history should produce
the same final state as a validator that has been running and updating since
activation.

# Specification

- Add a version field to the system ledger (dn.acme/ledger and friends).
- Do not allow the version field to be decreased.
- Add a new transaction type that updates the version field in the system ledger.
- Only allow that transaction to be executed against the DN ledger.
- When the transaction is executed against the DN ledger, push out a copy of it with the DN anchor.
- Make all new behavior dependent on the system ledger's version field.

# Implementation

TBD

# Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).