| AIP | Title                          | Status | Category | Author                                  | Created  |
| --- | ------------------------------ | ------ | -------- | --------------------------------------- | -------- |
| 008 | Additional transaction signers | Draft  | Core     | Ethan Reesor \<ethan.reesor@gmail.com\> | 20230831 |

# Summary

Allow the author of a transaction to specify additional signers that must sign a
transaction.

# Motivation

- To simplify supporting staking accounts with multiple authorities, as the
  staking system could simply require that the requests specify all the account
  authorities as required for the transaction.
- To enable additional workflows for organizations like the committees, as a
  transaction could be initiated such that it required additional sign-offs.
- To allows L2 systems to require specific authorities to sign a transaction
  without requiring them to be added to the account.

# Specification

* Require additional authorities, not specific signers (i.e. key books not
  pages)
* Requiring an additional authority behaves as if that authority was a required
  authority of the account
  * Signature requests are sent to the additional authorities
  * Authority signatures from the additional authorities are accepted, and
    required
* If a requested authority doesn't exist, the transaction is rejected
  * When a signature request is received, if the authority does not exist the
    network issues an abstention authority signature (with a memo)

# Implementation

* Add a field to the transaction header to specify additional authorities.
* Update the transaction-is-ready logic to add the additional authorities to the
  list.
* If an authority was disabled, override that (enable it for this transaction).
* Update the signature request producing logic to also produce requests for the
  additional authorities.
* If a signature request (for any authority) is sent to an account that doesn't
  exist, return an abstention with a memo that the account doesn't exist.

# Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).