# Accumulate Improvement Proposals (AIP) :gear:

This is the central location for collaboration on technical aspects and improvements of Accumulate.

The intention is to formalise specifications that implementors should adhere to when building on Accumulate in order to encourage ecosystem improvement and interoperability.

# Submitting a new AIP

**Every AIP must have a corresponding issue in [this repository][repo]**.

1. [Create an issue][new-issue]. The description must include the following:
   - **Summary**. Concisely summarize the proposed change.
   - **Motivation**. What is the motivation? Why does the protocol/its users
     need this feature?
   - **Behavior**. What is the desired behavior? How will the proposed feature
     behave?
   - **Implementation**. Optionally, how will it be implemented? This can be
     omitted initially, but it must be answered before the AIP is submitted.
2. Write an AIP. Once an issue has been created and vetted and an implementation
   plan has been agreed upon:
   - Fork this repository.
   - Write an AIP as per [AIP-X](AIP/x.md) using the [AIP Template](AIP/template.md).
   - Submit a merge request.
3. If the AIP is accepted, it will be voted on by the core development committee.
4. If the comittee ratifies the AIP, it will be scheduled for implementation.

[repo]: https://gitlab.com/accumulatenetwork/governance/aip
[new-issue]: https://gitlab.com/accumulatenetwork/governance/aip/-/issues/new

# AIP List

| AIP | Issue | Name                            | Category      | Status      | Version[^1] | Old number |
|-----|-------|---------------------------------|---------------|-------------|-------------|------------|
| 001 | #1    | Universal address format        | Ecosystem     | Live        |             |            |
| 032 | #32   | Executor versioning             | Core          | Live        | 1.1         | 002        |
| 031 | #31   | Advanced signing                | Signing       | Live        | 1.2         | 003        |
| 030 | #30   | Review periods                  | Signing       | Live        | 1.3         | 004        |
| 043 | #43   | RSV Ethereum signatures         | Signing       | Live        | 1.3         | 005        |
| 016 | #16   | Signature memos                 | Signing       | Live        | 1.3         | 006        |
| 033 | #33   | Reduce sub-ADI cost             | Transactions  | Live        | 1.3         | 007        |
| 012 | #12   | Additional transaction signers  | Authorization | Live        | 1.3         | 008        |
| 029 | #29   | Empty authority sets            | Authorization | Live        | 1.3         | 009        |
| 010 | #10   | Early transaction expiration    | Signing       | Live        | 1.3         |            |
| 035 | #35   | Credit purchase refunds         | Staking       | Accepted    |             | 011        |
| 049 | #49   | Proxy data entries              | Core          | Accepted    | 1.5         | 013        |
| 048 | #48   | Hashed time-locked operations   | Signing       | Accepted    | 1.5         |            |
| 050 | #50   | User-specified transaction fees | Signing       | Accepted    | 1.5         |            |

- **Accepted**: the proposal has been accepted and is ready for implementation.
- **Implemented**: the proposal has been implemented and is ready to be deployed.
- **Live**: the proposal has been implemented and deployed.

[^1]: Accepted changes that are scheduled for a version may slip into the next
version.

# Editors

Our current list of editors and curators:

-   **Ethan Reesor** - <ethan.reesor@gmail.com>

# Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).
