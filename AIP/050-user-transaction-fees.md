| AIP   | Title                            | Status | Category   | Author                                  | Created  |
| ----- | -------------------------------- | ------ | ---------- | --------------------------------------- | -------- |
| 050   | User-specified transaction fees  | Draft  | core       | Ethan Reesor \<ethan.reesor@gmail.com\> | 20240724 |

# Summary

When a user initiates a transaction, allow them to specify a fee that must be
paid as prerequisite to executing the transaction.

# Motivation

- **Exchange of assets.** This can be used as a mechanism to exchange tokens, or to exchange ownership of an account for tokens.
- **Service feeds.** This can be used by service providers as a way to collect fees in exchange for executing transactions on behalf of a user.

# Specification

The fee is submitted as if it is a signature and the tokens will be placed in escrow along with the active signature set. If the transaction expires, fee(s) paid will be returned. If the transaction executes but fails, fees will be returned. If the transaction executes and succeeds, fees will be distributed to the recipient account(s) specified by the original transaction.

# Implementation

TBD

# Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).
