| AIP   | Title                    | Status    | Category | Author                                  | Created  |
| ----- | ------------------------ | --------- | -------- | --------------------------------------- | -------- |
| 001   | Universal address format | Last Call | Core     | Ethan Reesor \<ethan.reesor@gmail.com\> | 20221115 |


# Summary

Create a unified address format that supports native Accumulate addresses,
Factoid addresses, Bitcoin P2PKH addresses, and Ethereum addresses.

# Motivation

First, to create a unified address format that can encode every kind of signing
hash that Accumulate supports.

Second, to create a verifiable (checksummed) address format users can use when
interacting with wallets to reduce risk (by passing hashes instead of public
keys) and minimize the danger of adding invalid entries to key pages.

# Specification

Accumulate supports multiple classes of address, each with a specific prefix:

| Prefix | Type
| ------ | --------------------------
| `AC`   | Accumulate public key hash
| `AS`   | Accumulate private key
| `FA`   | Factoid public key hash
| `Fs`   | Factoid private key
| `BT`   | BitCoin public key hash
| `0x`   | Ethereum public key hash
| `MH`   | Multihash

Wallets may optionally accept unprefixed BitCoin P2PKH addresses. However this
functionality is optional and will be removed if subsequent AIPs expand the
address format in a way that would conflict with the unprefixed BitCoin P2PKH
format.

The specification for Factoid and Ethereum addresses is identical to what those
protocols define. The specification for BitCoin addresses is identical except
for the additional `BT` prefix. P2PKH addresses are the only BitCoin addresses
supported at this type. If the protocol is updated to support additional BitCoin
signing mechanisms, support for those addresses must be added.

Accumulate addresses include a version number, so the full prefix is `AC1`. An
Accumulate v1 address is the hash of a key, plus a checksum, base58 encoded. See
the implementation for details.

Multihash addresses are a multihash-encoded hash, plus a checksum, multibase
base58 encoded. See the implementation for details. The identity multihash codec
indicates that the value is a hash value of an unknown type.

# Implementation

The `parseXX` functions are inverses of the respective format functions. AC and
MH use the same checksum scheme as FA and BitCoin addresses for consistency.

```go
func FormatAC1(hash []byte) string {
	return format2(hash, "AC1")
}

func FormatAS1(seed []byte) string {
	return format2(seed, "AS1")
}

func FormatFA(hash []byte) string {
	return format1(hash, 0x5f, 0xb1)
}

func FormatFs(seed []byte) string {
	return format1(seed, 0x64, 0x78)
}

func FormatBTC(hash []byte) string {
	return "BT" + format1(hash, 0x00)
}

func FormatETH(hash []byte) string {
	// Take the last 20 bytes
	if len(hash) > 20 {
		hash = hash[len(hash)-20:]
	}
	return "0x" + hex.EncodeToString(hash)
}

func FormatMH(hash []byte) string {
	// Encode with multihash
	b, _ := multihash.Encode(hash, multihash.IDENTITY)

	// Calculate a checksum
	c := make([]byte, len(b)+2)
	copy(c, "MH")
	copy(c[2:], b)
	checksum := sha256.Sum256(c)
	checksum = sha256.Sum256(checksum[:])

	// Encode with multibase base58
	b = append(b, checksum[:4]...)
	s, _ := multibase.Encode(multibase.Base58BTC, b)
	return "MH" + s
}

func format1(hash []byte, prefix ...byte) string {
	// Add the prefix
	b := make([]byte, len(prefix)+len(hash)+4)
	n := copy(b, prefix)
	n += copy(b[n:], hash)

	// Add the checksum
	checksum := sha256.Sum256(b)
	checksum = sha256.Sum256(checksum[:])
	copy(b[n:], checksum[:4])

	// Encode
	return base58.Encode(b)
}

func format2(hash []byte, prefix string) string {
	// Add the prefix
	b := make([]byte, len(prefix)+len(hash)+4)
	n := copy(b, prefix)
	n += copy(b[n:], hash)

	// Add the checksum
	checksum := sha256.Sum256(b)
	checksum = sha256.Sum256(checksum[:])
	copy(b[n:], checksum[:4])

	// Encode the hash and checksum and add the prefix
	return prefix + base58.Encode(b[len(prefix):])
}

func Parse(s string) (Address, error) {
	if len(s) < 2 {
		return nil, errors.New("invalid address: too short")
	}

	code, s := s[:2], s[2:]
	switch code {
	case "AC":
		return parseAC(s)
	case "AS":
		return parseAS(s)
	case "FA":
		return parseFA(s)
	case "Fs":
		return parseFs(s)
	case "BT":
		return parseBT(s)
	case "0x":
		return parse0x(s)
	case "MH":
		return parseMH(s)
	}

	// Optional
	b, err := base58.Decode(s)
	if err == nil && b[0] == '1' {
		return &BTCAddress{Value: b}
	}

	return nil, fmt.Errorf("%q is not a supported address format", s)
}
```

# Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).
